﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab2.src;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            //StateMachine sm = new StateMachine();
            //string input = "if abc<1.1.5 + 2 - a th en q=1+3*a;";
            //var result = sm.Execute(input);
            //Console.WriteLine(input);
            //Console.WriteLine();
            //foreach (var pair in result)
            //{
            //    Console.WriteLine("\"{0}\" : {1} : {2}",pair.Item3, pair.Item1, pair.Item2);
            //}
            //Console.Read();

            Grammar grammar = new Grammar();

            IEnumerable<GrammarRule> rules = new List<GrammarRule>() {
                new GrammarRule(new Nonterminal("A"), new List<Alternative>() {
                    new Alternative() {
                        new Nonterminal("B")
                    }
                }, true),
                new GrammarRule(new Nonterminal("B"), new List<Alternative>() {
                    new Alternative() {
                        new Terminal("for"),
                        new Terminal("("),
                        new Nonterminal("FOR_CONDITIONS"),
                        new Terminal(")"),
                        new Terminal("{"),
                        new Nonterminal("FOR_BODY"),
                        new Terminal("}"),
                    }
                }),
                new GrammarRule(new Nonterminal("FOR_CONDITIONS"), new List<Alternative>() {
                    new Alternative() {
                        new Nonterminal("C1"),
                        new Terminal(","),
                        new Nonterminal("C2"),
                        new Terminal(","),
                        new Nonterminal("C3")
                    }
                }),
                new GrammarRule(new Nonterminal("H"), new List<Alternative>() {
                    new Alternative() {
                        new Nonterminal("Variable"),
                        new Nonterminal("HH"),
                    },
                }),
                new GrammarRule(new Nonterminal("HH"), new List<Alternative>() {
                    new Alternative() {
                        new Terminal("["),
                        new Nonterminal("HR"),
                        new Terminal("]"),
                    },
                    new Alternative() {
                        new Empty()
                    }
                }),
                new GrammarRule(new Nonterminal("HR"), new List<Alternative>() {
                    new Alternative() {
                        new Nonterminal("H"),
                    },
                    new Alternative() {
                        new Nonterminal("Variable"),
                    },
                    new Alternative() {
                        new Nonterminal("Number"),
                    },
                }),
                new GrammarRule(new Nonterminal("C1"), new List<Alternative>() {
                    new Alternative() {
                        new Nonterminal("Primitive_type"),
                        new Nonterminal("Variable"),
                        new Nonterminal("Assign"),
                        new Nonterminal("HR"),
                        new Nonterminal("CC1"),
                    },
                    new Alternative() {
                        new Empty(),
                    },
                }),
                new GrammarRule(new Nonterminal("CC1"), new List<Alternative>() {
                    new Alternative() {
                        new Terminal(","),
                        new Nonterminal("C1"),
                    },
                    new Alternative() {
                        new Empty(),
                    },
                }),
                new GrammarRule(new Nonterminal("C2"), new List<Alternative>() {
                    new Alternative() {
                        new Nonterminal("HR"),
                        new Nonterminal("Logical_operator"),
                        new Nonterminal("C1"),
                    },
                    new Alternative() {
                        new Empty(),
                    },
                }),
                new GrammarRule(new Nonterminal("C3"), new List<Alternative>() {
                    new Alternative() {
                        new Nonterminal("H"),
                        new Nonterminal("C3C"),
                        new Nonterminal("CC3"),
                    },
                    new Alternative() {
                        new Empty(),
                    },
                }),
                new GrammarRule(new Nonterminal("C3C"), new List<Alternative>() {
                    new Alternative() {
                        new Nonterminal("Increment"),
                    },
                    new Alternative() {
                        new Nonterminal("Math_operator"),
                    },
                    new Alternative() {
                        new Nonterminal("Math_operator"),
                        new Nonterminal("C3A")
                    },
                    new Alternative() {
                        new Nonterminal("C3A"),
                        new Nonterminal("C3M"),
                    },
                }),
                new GrammarRule(new Nonterminal("C3A"), new List<Alternative>() {
                    new Alternative() {
                        new Nonterminal("Assign"),
                        new Nonterminal("HR"),
                    },
                }),
                new GrammarRule(new Nonterminal("C3M"), new List<Alternative>() {
                    new Alternative() {
                        new Nonterminal("Math_operator"),
                        new Nonterminal("HR"),
                    },
                    new Alternative() {
                        new Empty()
                    },
                }),
                new GrammarRule(new Nonterminal("CC3"), new List<Alternative>() {
                    new Alternative() {
                        new Terminal(","),
                        new Nonterminal("C3"),
                    },
                    new Alternative() {
                        new Empty()
                    },
                }),
                new GrammarRule(new Nonterminal("FOR_BODY"), new List<Alternative>() {
                    new Alternative() {
                        new Nonterminal("H"),
                        new Nonterminal("C3C"),
                        new Nonterminal("FOR_BODY"),
                    },
                }),
            };

            grammar.AddRange(rules);

            SyntacticAnalyzer sa = new SyntacticAnalyzer(grammar);

            Console.WriteLine("nonterminals");
            foreach(Nonterminal n in sa.NonterminalAlphabet)
            {
                Console.WriteLine(n);
            }
            Console.WriteLine("terminals");
            foreach(Terminal t in sa.TerminalAlphabet)
            {
                Console.WriteLine(t);
            }
            Console.WriteLine("FIRST");
            foreach(var entry in sa.FirstSet)
            {
                Console.WriteLine(entry);
            }
            Console.WriteLine("FOLLOW");
            foreach(var entry in sa.FollowSet)
            {
                Console.WriteLine(entry);
            }

            Console.WriteLine();
            Console.WriteLine(sa.CommandTable);

            Console.Read();
        }
    }
}
