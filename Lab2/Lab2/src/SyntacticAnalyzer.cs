﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.src
{
    class SyntacticAnalyzer
    {

        public Grammar LangGrammar { get; set; }
        public Set FirstSet { get; set; }
        public Set FollowSet { get; set; }
        public Table CommandTable { get; set; }
        public IEnumerable<Terminal> TerminalAlphabet { get; set; }
        public IEnumerable<Nonterminal> NonterminalAlphabet { get; set; }

        public SyntacticAnalyzer()
        {
           
            
        }

        public SyntacticAnalyzer(Grammar grammar)
        {
            LangGrammar = grammar;
            TerminalAlphabet = grammar.GetTerminalAlphabet();
            NonterminalAlphabet = grammar.GetNonterminalAlphabet();
            FirstSet = GetFirst();
            FollowSet = GetFollow();
            CommandTable = GetTable();
        }

        public bool Analyze(IEnumerable<Terminal> input) {
            List<Terminal> inputList = input.ToList();
            Stack<Symbol> stack = new Stack<Symbol>();
            stack.Push(new End());
            stack.Push(LangGrammar.GetStarter());

            while(!inputList[0].Equals(new End()) && !stack.Peek().Equals(new End()))
            {
                Terminal s = input.ElementAt(0);
                Symbol v = stack.Pop();

                if(v is Terminal)
                {
                    if (v.Equals(s))
                    {
                        inputList.RemoveAt(0);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    foreach(var nonterm in CommandTable[v as Nonterminal][s])
                    {
                        stack.Push(nonterm);
                    }
                }

            }

            return false;
        }

        private Set GetFirst() {
            Set first = new Set();
            foreach(Nonterminal nonterminal in NonterminalAlphabet) {
                first.Add(new SetEntry(nonterminal, new List<Terminal>()));
            }
            bool changed = true;
            while (changed)
            {
                changed = false;
                foreach (GrammarRule rule in LangGrammar)
                {
                    foreach(Alternative alternative in rule.Alternatives)
                    {
                        var terminals = first.GetTerminals(rule.Left);
                        var difference = First(alternative.ToArray()).Except(terminals);
                        terminals.AddRange(difference);
                        changed = difference.Count() != 0 ? true : changed;
                    }
                }
            }
            return first;
        }

        private IEnumerable<Terminal> First(params Symbol[] symbols)
        {
            List<Terminal> first = new List<Terminal>();
            if(symbols.Length == 1)
            {
                Symbol symbol = symbols.First();
                if(symbol is Terminal)
                {
                    first.Add(symbol as Terminal);
                    return first;
                }
                else if(symbol is Nonterminal)
                {
                    Nonterminal nonterminal = symbol as Nonterminal;
                    if (LangGrammar.GetAlternatives(nonterminal).Any(alternative => alternative.ContainsEmpty()))
                    {
                        first.Add(new Empty());
                    }

                    foreach (GrammarRule rule in LangGrammar)
                    {
                        foreach(Alternative alternative in rule.Alternatives)
                        {
                            first.AddRange(First(alternative.ToArray()));
                            if (!alternative.ContainsEmpty())
                            {
                                return first;
                            }
                        }
                    }
                }
            }
            else
            {
                foreach(Symbol symbol in symbols)
                {
                    IEnumerable<Terminal> partialFirst = First(symbol);
                    first.AddRange(partialFirst);
                    if (!partialFirst.Any(terminal => terminal is Empty))
                    {
                        break;
                    }
                }

            }
            return first;
        }

        private Set GetFollow() {
            Set follow = new Set();

            foreach(Nonterminal nonterminal in NonterminalAlphabet) {
                follow.Add(new SetEntry(nonterminal, new List<Terminal>()));
            }

            // follow[s] = $
            var starter = LangGrammar.FirstOrDefault(rule => rule.IsStarter).Left;
            if (starter != null)
            {
                follow[starter].Add(new End());
            }

            bool changed = true;
            while (changed)
            {
                changed = false;
                foreach(GrammarRule rule in LangGrammar)
                {
                    foreach(Alternative alternative in rule.Alternatives)
                    {
                        // iterating through the chain
                        for (int i = 0; i < alternative.Count; i++)
                        {
                            List<Terminal> difference = new List<Terminal>();
                            Nonterminal currentSymbol = alternative[i] as Nonterminal;
                            if (currentSymbol != null)
                            {
                                // element is not the last one
                                if (alternative.Count - i > 1)
                                {
                                    Symbol nextSymbol = alternative[i + 1];
                                    string next = nextSymbol.Name;
                                    string current = alternative[i].Name;

                                    if (nextSymbol as Nonterminal != null)
                                    {
                                        Nonterminal nextNonterminal = nextSymbol as Nonterminal;
                                        difference.AddRange(FirstSet[nextNonterminal].Except(follow[currentSymbol]).ToList());
                                        if (FirstSet[nextNonterminal].Any(terminal => terminal is Empty))
                                        {
                                            difference.AddRange(follow[rule.Left].Except(follow[currentSymbol]));
                                        }
                                    }
                                    else if (!follow[currentSymbol].Any(terminal => terminal.Equals(nextSymbol)))
                                    {
                                        difference.Add(nextSymbol as Terminal);
                                    }
                                }
                                // element is the last one
                                else
                                {
                                    difference.AddRange(follow[rule.Left].Except(follow[currentSymbol]));
                                }
                                follow[currentSymbol].AddRange(difference);
                                changed = changed ? changed : difference.Count > 0;
                            }
                        }
                    }
                }
            }

            return follow;
        }

        private Table GetTable()
        {
            Table table = new Table();
            List<TableCell> row = TerminalAlphabet.
                Select(terminal => new TableCell(terminal, new List<Symbol>()))
                .ToList();

            NonterminalAlphabet.ToList().ForEach(nonterminal => table.Add(new TableEntry(nonterminal, row)));

            foreach(GrammarRule rule in LangGrammar)
            {
                foreach(Alternative alternative in rule.Alternatives)
                {
                    var terminals = FirstSet[rule.Left];

                    foreach(Terminal a in terminals)
                    {
                        table[rule.Left][a].AddRange(alternative);
                    }

                    if (FirstSet[rule.Left].Any(term => term is Empty))
                    {
                        foreach(Terminal b in FollowSet[rule.Left])
                        {
                            table[rule.Left][b].AddRange(alternative);
                        }
                        if(FollowSet[rule.Left].Any(term => term is End))
                        {
                            table[rule.Left][new Empty()].AddRange(alternative);
                        }
                    }
                }
            }
            return table;
        }
    }

    abstract class Symbol {

        public virtual string Name { get; set; }

        public override bool Equals(object obj)
        {
            Symbol symbol = obj as Symbol;
            return symbol == null ? false : Name.Equals(symbol.Name);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override string ToString()
        {
            return Name;
        }

    }

    class Terminal : Symbol {

        public Terminal()
        {
            Name = "";
        }

        public Terminal(string name)
        {
            Name = name;
        }

        public Tokens Token { get; set; }

        //public override string Name
        //{
        //    get
        //    {
        //        return Token.ToString();
        //    }

        //    set
        //    {
        //        this.Token = (Tokens) Enum.Parse(typeof(Tokens), value);
        //    }
        //}
    }

    class Nonterminal : Symbol {

        public Nonterminal(string name)
        {
            this.Name = name;
        }

        public override string Name
        {
            get
            {
                return base.Name;
            }

            set
            {
                base.Name = value;
            }
        }
    }

    class Empty : Terminal {

        public Empty()
        {
            Name = "empty";
        }
    }

    class End : Terminal
    {
        public End()
        {
            Name = "end";
        }
    }

    class Alternative : List<Symbol> {
        public bool ContainsEmpty()
        {
            return this.Any(symbol => symbol is Empty);
        }
    }

    class GrammarRule {
        public Nonterminal Left { set; get; }
        public IEnumerable<Alternative> Alternatives { get; set; }
        public bool IsStarter { get; set; }

        public GrammarRule(Nonterminal left, IEnumerable<Alternative> alternatives, bool isStarter = false)
        {
            this.Left = left;
            this.Alternatives = alternatives;
            this.IsStarter = isStarter;
        }
    }

    class Grammar : List<GrammarRule>
    {
        public IEnumerable<Alternative> GetAlternatives(Nonterminal nonterminal)
        {
            return this.First(rule => rule.Left.Equals(nonterminal)).Alternatives;
        }

        public IEnumerable<Nonterminal> GetNonterminalAlphabet()
        {
            return this.Select(rule => rule.Left);
        }
        
        public IEnumerable<Terminal> GetTerminalAlphabet()
        {
            var result = this.SelectMany(rule => rule.Alternatives)
                .SelectMany(alternative => alternative)
                .Where(symbol => symbol is Terminal)
                .Cast<Terminal>()
                .Distinct()
                .ToList();
            result.Add(new End());
            return result;
        }

        public Nonterminal GetStarter()
        {
            return this.FirstOrDefault(rule => rule.IsStarter).Left;
        }
    }

    class TableCell : Tuple<Terminal, List<Symbol>>
    {
        public TableCell(Terminal terminal, List<Symbol> symbol) : base(terminal, symbol) { }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            result.Append(Item1 + ": ");

            foreach (Symbol cell in Item2)
            {
                result.Append(cell.ToString() + ", ");
            }
            return result.ToString();
        }

    }

    class TableEntry : Tuple<Nonterminal, List<TableCell>>{

        public TableEntry(Nonterminal nonterminal, List<TableCell> replacements) : base(nonterminal, replacements)
        {

        } 

        public List<Symbol> this[Terminal terminal]
        {
            get
            {
                return this.Item2.FirstOrDefault(cell => cell.Item1.Equals(terminal)).Item2;
            }
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            result.Append(Item1 + ": [");

            foreach(TableCell cell in Item2)
            {
                result.Append(cell.ToString() + ", ");
            }
            result.Append("] | ");
            result.AppendLine();
            return result.ToString();
        }

    }

    class Table : List<TableEntry>
    {
        public TableEntry this[Nonterminal nonterminal]
        {
            get
            {
                return this.FirstOrDefault(entry => entry.Item1.Equals(nonterminal));
            }
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();

            foreach(TableEntry entry in this)
            {
                result.AppendLine(entry.ToString());
            }

            return result.ToString();
        }
    }

    class SetEntry : Tuple<Nonterminal, List<Terminal>>
    {
        public SetEntry(Nonterminal nonterminal, List<Terminal> terminals) : base(nonterminal, terminals)
        {
            
        }

        public override string ToString()
        {
            string terminals = "{";
            foreach(var terminal in Item2)
            {
                terminals += terminal + " ";
            }
            terminals += "}";
            return "{Nonterminal: " + Item1 + Environment.NewLine + "terminals: " + terminals + Environment.NewLine;
        }
    }

    class Set : List<SetEntry>
    {
        public List<Terminal> this[Nonterminal nonterminal]
        {
            get {
                return this.FirstOrDefault(entry => entry.Item1.Equals(nonterminal)).Item2;
            }
        }
        public List<Terminal> GetTerminals (Nonterminal nonterminal)
        {
                return this.First(entry => entry.Item1.Equals(nonterminal)).Item2;
        }
    }
}