﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.src
{
    class StateMachine
    {

        private StateTable stateTable { get; set; }

        private List<Tuple<Tokens, Dictionary<int, string>>>   valuesTable;
        
        public StateMachine()
        {

            valuesTable = new List<Tuple<Tokens,Dictionary<int,string>>>();
            foreach(Tokens token in Enum.GetValues(typeof(Tokens))){
                valuesTable.Add(new Tuple<Tokens,Dictionary<int,string>>(token, new Dictionary<int, string>()));
            }

            List<State> states =  new List<State> 
            {
                new State(0, false, Tokens.None),
                new State(1, false, Tokens.None),
                new State(2, false, Tokens.None),
                new State(3, false, Tokens.None),
                new State(4, true, Tokens.Else),
                new State(5, true, Tokens.Variable),
                new State(6, false, Tokens.None),
                new State(7, true, Tokens.If),
                new State(8, false, Tokens.None),
                new State(9, false, Tokens.None),
                new State(10, false, Tokens.None),
                new State(11, true, Tokens.Then),
                new State(12, true, Tokens.Operator),
                new State(13, true, Tokens.Number),
                new State(14, false, Tokens.None),
                new State(15, true, Tokens.Number),
                new State(16, false, Tokens.None),
				new State(17, true, Tokens.Number),
				new State(18, false, Tokens.None)
            };
            List<Tuple<int, PathList>> table = new List<Tuple<int, PathList>>
            {
                new Tuple<int,PathList>(0, new PathList {
                    new Path(new Alphabet("cC^(iet)"),5),
                    new Path(new Alphabet("i"),6),
                    new Path(new Alphabet("e"),1),
                    new Path(new Alphabet("t"),8),
                    new Path(new Alphabet("@"),12), //operators
                    new Path(new Alphabet("d"),13)
                }),
                new Tuple<int,PathList>(1, new PathList {
                    new Path(new Alphabet("l"),2),
                    new Path(new Alphabet("cCd^(l)"),5)
                }),
                new Tuple<int,PathList>(2, new PathList {
                    new Path(new Alphabet("s"),3),
                    new Path(new Alphabet("cCd^(s)"),5)
                }),
                new Tuple<int,PathList>(3, new PathList {
                    new Path(new Alphabet("e"),4),
                    new Path(new Alphabet("cCd^(e)"),5)
                }),
                new Tuple<int,PathList>(4, new PathList {
                    new Path(new Alphabet("cCd"),5)
                }),
                new Tuple<int,PathList>(5, new PathList {
                    new Path(new Alphabet("cCd"),5)
                }),
                new Tuple<int,PathList>(6, new PathList {
                    new Path(new Alphabet("cCd^(f)"),5),
                    new Path(new Alphabet("f"),7)
                }),
                new Tuple<int,PathList>(7, new PathList {
                    new Path(new Alphabet("cCd"),5)
                }),
                new Tuple<int,PathList>(8, new PathList {
                    new Path(new Alphabet("cCd^(h)"),5),
                    new Path(new Alphabet("h"),9)
                }),
                new Tuple<int,PathList>(9, new PathList {
                    new Path(new Alphabet("cCd^(e)"),5),
                    new Path(new Alphabet("e"),10)
                }),
                new Tuple<int,PathList>(10, new PathList {
                    new Path(new Alphabet("cCd^(n)"),5),
                    new Path(new Alphabet("n"),11)
                }),
                new Tuple<int,PathList>(11, new PathList {
                    new Path(new Alphabet("cCd^(f)"),5)
                }),
                new Tuple<int,PathList>(12, new PathList {

                }),
                new Tuple<int,PathList>(13, new PathList {
                    new Path(new Alphabet("d"),13),
                    new Path(new Alphabet("."),14)
                }),
                new Tuple<int,PathList>(14, new PathList {
                    new Path(new Alphabet("d"),15)
                }),
                new Tuple<int,PathList>(15, new PathList {
                    new Path(new Alphabet("d"),15),
                    new Path(new Alphabet("E"),16)
                }),
                new Tuple<int,PathList>(16, new PathList {
                    new Path(new Alphabet("d"),17),
                }),
                new Tuple<int,PathList>(17, new PathList {
                    new Path(new Alphabet("d"),17),
                    new Path(new Alphabet("cC"),18)
                }),
                new Tuple<int,PathList>(18, new PathList())
            };
            this.stateTable = new StateTable(states, table);
        }

        public List<Tuple<Tokens,int, string>> Execute(string input)
        {
            List<Tuple<Tokens, int, string>> result = new List<Tuple<Tokens, int, string>>();

            string currentValue = "";
            StateChangeResult changeResult;

            for (int i = 0; i < input.Length; i++)
            {
                char ch = input[i];
                changeResult = stateTable.ChangeState(ch);
                // can't go farther
                if (!changeResult.WasSuccessful)
                {

                    if (!currentValue.Equals("")) {
                        // check if this item is already in our table
                        // find dictionary for this token
                         var dict = valuesTable
                            .FirstOrDefault(tokenValues => tokenValues.Item1 == changeResult.Token) 
                            .Item2;
                        // find according value in it
                        KeyValuePair<int,string>? value = dict.Cast<KeyValuePair<int,string>?>()
                            .FirstOrDefault(keyVal => keyVal.Value.Value.Equals(currentValue));

                        int id = 0;
                        if (value == null)
                        {
                            id = dict.Count;
                            dict.Add(id, currentValue);
                        }
                        else
                        {
                            id = value.Value.Key;
                        }
                        Tuple<Tokens, int, string> item = new Tuple<Tokens, int, string>(changeResult.Token, id, currentValue);
                        result.Add(item);

                        i--;
                    }
                    currentValue = "";

                }
                // going farther
                else
                {
                    currentValue += ch;
                }
            }

            return result;
        }
    }
}
