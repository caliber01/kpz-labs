﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab2.src
{

    class StateTable
    {

        private List<Tuple<int, PathList>> stateTable;
        private List<State> states;
        private int initialStateId;
        private int currentStateId;

        public StateTable()
        {

        }

        public StateTable(List<State> states, List<Tuple<int, PathList>> stateTable)
        {
            this.states = states;
            this.stateTable = stateTable;
			initialStateId = states.First ().Id;
        }

        public StateChangeResult ChangeState(char symbol)
        {
            var changePath = stateTable.FirstOrDefault(from => from.Item1 == currentStateId)
                .Item2.FirstOrDefault(path => path.By.Any(by => by == symbol));
            // if havent find a path using this symbol, then return Tokens.None, valid Token else
            int resultId = changePath == null ? -1 : changePath.DestinationId;
            
            Tokens corrToken = states.FirstOrDefault(state => state.Id == currentStateId).Token;
            if (resultId == -1)
            {
                ResetCurrentState();
            }
            else
            {
                currentStateId = resultId;
            }

            return new StateChangeResult(resultId != -1, corrToken);
        }

        public void ResetCurrentState() 
        {
            currentStateId = initialStateId;
        }
    }

    class StateChangeResult
    {

        public bool WasSuccessful { get; set; }
        public Tokens Token { get; set; }

        public StateChangeResult()
        {

        }

        public StateChangeResult(bool wasSuccessful, Tokens token)
        {
            WasSuccessful = wasSuccessful;
            Token = token;
        }
    }

    // for convenient statetable inizialization
    class StateChange 
    {

        public StateChange()
        {

        }

        public StateChange(int fromId, int toId, Alphabet by)
        {
            SourceId = fromId;
            DestinationId = toId;
            By = by;
        }

        public int SourceId { get; set; }
        public int DestinationId { get; set; }
        public Alphabet By { get; set; }
    }

    class PathList : List<Path>
    {

    }

    class Path
    {
        public Path()
        {

        }

        public Path(Alphabet by, int destinationId)
        {
            By = by;
            DestinationId = destinationId;
        }

        public int DestinationId { get; set; }
        public Alphabet By { get; set; }
    }

    class Alphabet : List<char>
    {
        public Alphabet()
        {

        }
        public Alphabet(char from)
        {
            this.Add(from);
        }
        public Alphabet(string from)
        {
			// cCd^(f)
			for (int i = 0; i < from.Length; i++) {
				char c = from[i];
				switch (c) {
				    case 'c':
					    AddLowChars ();
					    break;
				    case 'C':
					    AddUpChars ();
				    break;
			        case 'd':
				        AddDigits ();
				        break;
			        case '^':
				        string exclude = "";
				        i += 2;
				        while (from [i] != ')') {
					        exclude += from [i];
					        i++;
				        }
				        Exclude (exclude);
				        break;
                    case '@':
                        AddOperators();
                        break;
				    default:
					    this.Add (c);
					    break;
				}
			}
        }
		private void AddLowChars(){
			for (char c = 'a'; c <= 'z'; c++) {
				this.Add (c);
			}
		}
		private void AddUpChars(){
			for (char c = 'A'; c <= 'Z'; c++) {
				this.Add (c);
			}
		}
		private void AddDigits(){
			for (char c = '0'; c <= '9'; c++) {
				this.Add (c);
			}
		}
        private void AddOperators()
        {
            this.AddRange("+-*^&|/=");
        }
		private void Exclude(string chars){
            foreach (char c in chars)
            {
                this.Remove(c);
            }
		}
    }
}
