﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.src
{
    enum Tokens{None, If, Else, Then, Number, Variable, Symbol, Operator}

    class State
    {

        public int Id { get; set; }
        public bool IsFinish { get; set; }
        public Tokens Token;

        public State()
        {

        }

        public State(int id, bool finish, Tokens token)
        {
            Id = id;
            IsFinish = finish;
            Token = token;
        }
    }
}
